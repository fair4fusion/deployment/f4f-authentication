import os
import keycloak

class KeycloakAdmin:

  def __init__(self):
    self.keycloak_url = os.environ.get("KEYCLOAK_URL")
    self.keycloak_roles_admin_username = os.environ.get('KEYCLOAK_ROLES_ADMIN_USERNAME')
    self.keycloak_roles_admin_password = os.environ.get('KEYCLOAK_ROLES_ADMIN_PASSWORD')
    self.keycloak_realm = os.environ.get('KEYCLOAK_REALM')
    self.keycloak_client = os.environ.get('KEYCLOAK_CLIENT')
    self.update_keycloak_admin()
    if self.keycloak_client is not None:
      self.keycloak_client_id = self.keycloak_admin.get_client_id(self.keycloak_client)
    else:
      self.keycloak_client_id = None

  def update_keycloak_admin(self):
    self.keycloak_admin = keycloak.KeycloakAdmin(
        server_url=self.keycloak_url,
        username=self.keycloak_roles_admin_username,
        password=self.keycloak_roles_admin_password,
        realm_name=self.keycloak_realm,
        verify=True
      )

  def get_client_roles(self, user_id):
    tries = 5
    while tries>0:
      try:
        roles = self.keycloak_admin.get_client_roles_of_user(user_id, self.keycloak_client_id)
        return [role['name'] for role in roles]
      except:
        self.update_keycloak_admin()
        tries += 1
    raise Exception('Cannot connect to keycloak')

  def has_client_role(self, user_id, role):
    return role in self.get_client_roles(user_id)

  def get_groups(self, user_id):
    tries = 5
    while tries>0:
      try:
        groups = self.keycloak_admin.get_user_groups(user_id)
        return [group['name'] for group in groups]
      except:
        self.update_keycloak_admin()
        tries+=1
    raise Exception('Cannot connect to keycloak')

  def get_group(self, user_id, valid_groups):
    groups = self.get_groups(user_id)
    for group in groups:
      if group in valid_groups:
        return group
    if 'default' in valid_groups:
      return 'default'
    return None

