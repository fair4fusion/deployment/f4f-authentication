import os
roles_admin_password = os.environ['ROLES_ADMIN_PASSWORD']
roles_admin_username = os.environ['ROLES_ADMIN_USERNAME']
config_dir = os.environ['CONFIG_DIR']

import random
import base64
from backports.pbkdf2 import pbkdf2_hmac

hash_iterations = 27500
salt_size = 16
hashed_salted_size = 64
salt = bytes(random.getrandbits(8) for _ in range(salt_size))
hashed_salted = pbkdf2_hmac("sha256", roles_admin_password.encode('utf-8'), salt, 27500, hashed_salted_size)

import json

config = {
    "id": "f4f",
    "realm": "f4f",
    "enabled" : True,
    "accessTokenLifespan": 86400,
    "ssoSessionIdleTimeout": 2592000,
    "ssoSessionMaxLifespan": 2592000,
    "offlineSessionIdleTimeout": 2592000,
    "roles": {},
    "clients": [],
    "users": [{
    "username" : roles_admin_username,
    "enabled": True,
    "credentials" : [ {
        "type" : "password",
        "hashedSaltedValue" : base64.b64encode(hashed_salted).decode('utf-8'),
        "salt" : base64.b64encode(salt).decode('utf-8'),
        "hashIterations" : hash_iterations,
        "counter" : 0,
        "algorithm" : "pbkdf2-sha256",
        "digits" : 0,
        "period" : 0,
        "createdDate" : 1576833701648,
        "config" : { }
    } ],
    "clientRoles" : {
        "realm-management" : [ "view-users", "manage-clients" ]
    }
    }]
}

if ( os.environ.get('F4F_REST_OP_API_CLIENT_SECRET') is not None) and ( os.environ.get('F4F_REST_OP_API_REDIRECT_URI') is not None):
    f4f_client_secret = os.environ['F4F_REST_OP_API_CLIENT_SECRET']
    f4f_redirect_uri = os.environ['F4F_REST_OP_API_REDIRECT_URI']
    new_client = {
        "clientId": "f4f-rest-op-api",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : f4f_client_secret,
        "redirectUris" : [ f"http://{f4f_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)

    # config['roles']={
    #     "client": {
    #         "minio": [{
    #         "name": "write"
    #         }]
    #     }
    # }
else:
    print("Warning! Skipping creating keycloak client for f4f rest op api")

if ( os.environ.get('F4F_UI_CLIENT_SECRET') is not None) and ( os.environ.get('F4F_UI_REDIRECT_URI') is not None):
    f4f_ui_secret = os.environ['F4F_UI_CLIENT_SECRET']
    f4f_ui_redirect_uri = os.environ['F4F_UI_REDIRECT_URI']
    new_client = {
        "clientId": "f4f-ui",
        "enabled": True,
        "clientAuthenticatorType" : "client-secret",
        "secret" : f4f_ui_secret,
        "redirectUris" : [ f"http://{f4f_ui_redirect_uri}/*" ]
    }
    config['clients'].append(new_client)

    # config['roles']={
    #     "client": {
    #         "minio": [{
    #         "name": "write"
    #         }]
    #     }
    # }
else:
    print("Warning! Skipping creating keycloak client for f4f rest op api")    
with open(config_dir+'config.json', 'w') as outfile:
    json.dump(config, outfile)