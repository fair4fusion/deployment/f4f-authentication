import os
keycloak_host_global = os.environ['KEYCLOAK_HOST_GLOBAL']
keycloak_realm = os.environ['KEYCLOAK_REALM']
keycloak_client_id = os.environ['KEYCLOAK_CLIENT_ID']
keycloak_client_secret = os.environ['KEYCLOAK_CLIENT_SECRET']
keycloak_redirect_uri = os.environ['KEYCLOAK_REDIRECT_URI']
secrets_dir = os.environ['SECRETS_DIR']

import json

secrets = {
    "web": {
        "issuer": f"https://{keycloak_host_global}/auth/realms/{keycloak_realm}",
        "auth_uri": f"https://{keycloak_host_global}/auth/realms/{keycloak_realm}/protocol/openid-connect/auth",
        "client_id": keycloak_client_id,
        "client_secret": keycloak_client_secret,
        "redirect_uris": [
            keycloak_redirect_uri
        ],
        "userinfo_uri": f"https://{keycloak_host_global}/auth/realms/{keycloak_realm}/protocol/openid-connect/userinfo", 
        "token_uri": f"https://{keycloak_host_global}/auth/realms/{keycloak_realm}/protocol/openid-connect/token",
        "token_introspection_uri": f"https://{keycloak_host_global}/auth/realms/{keycloak_realm}/protocol/openid-connect/token/introspect",
        "logout_uri": f"https://{keycloak_host_global}/auth/realms/precmed/protocol/openid-connect/logout",
        "bearer_only": "true"
    }
}

with open(secrets_dir+'client_secrets.json', 'w') as outfile:
    json.dump(secrets, outfile)